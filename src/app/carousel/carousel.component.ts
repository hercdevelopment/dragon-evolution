import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';

@Component({
  selector: 'app-carousel',
  templateUrl: './carousel.component.html',
  styleUrls: ['./carousel.component.css']
})
export class CarouselComponent implements OnInit {

  public slides: string[];
  public selectedEpisode = 1;

  public episode1: string[] = [
    '../assets/images/Episode1/Page1.jpg',
    '../assets/images/Episode1/Page2.jpg',
    '../assets/images/Episode1/Page3.jpg',
    '../assets/images/Episode1/Page4.jpg',
    '../assets/images/Episode1/Page5.jpg',
    '../assets/images/Episode1/Page6.jpg',
  ]

  public episode2: string[] = [
    '../assets/images/Episode2/Page1.jpg',
    '../assets/images/Episode2/Page2.jpg',
    '../assets/images/Episode2/Page3.jpg',
    '../assets/images/Episode2/Page4.jpg',
    '../assets/images/Episode2/Page5.jpg',
    '../assets/images/Episode2/Page6.jpg',
    '../assets/images/Episode2/Page7.jpg'
  ]

  i: number = 0;

  showSlide(slides : string[], i : number) {
    let slide = slides[i];
    return slide;
  }

  getPrev(slides: string[], i: number) {
    this.i = this.i - 1;
    if (this.i <= 0) {
      this.i = 5;
    }
    this.showSlide(slides, i)
  }

  getNext(slides: string[], i: number) {
    this.i = this.i + 1;
    if (this.i == this.slides.length) {
      this.i = 0;
    }
    this.showSlide(slides, i)
  }

  constructor() {
    this.slides = this.episode1;
  }

  ngOnInit(): void {
    
  }

  public changeEpisode(selectedEpisode : number) {

    if (selectedEpisode == 1) {
      this.slides = this.episode1;
      this.selectedEpisode = selectedEpisode;
    }

    if (selectedEpisode == 2) {
      this.slides = this.episode2;
      this.selectedEpisode = selectedEpisode;
    }

    console.log(this.selectedEpisode);

  }



}
