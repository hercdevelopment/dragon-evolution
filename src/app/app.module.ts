import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { BodyComponent } from './body/body.component';
import { CarouselComponent } from './carousel/carousel.component';
import { TeamComponent } from './team/team.component';
import { AccordionModule } from './accordion/accordion.module';
import { FAQComponent } from './faq/faq.component';
import { UtilitiesComponent } from './utilities/utilities.component';
import { FooterComponent } from './footer/footer.component';
import { AppRoutingModule } from './app-routing.module';
import { LandingPageComponent } from './landing-page/landing-page.component';
import { ResizeDetectorDirective } from './resize-detector.directive';
import { VerticalTimelineComponent } from './vertical-timeline/vertical-timeline.component';
import { RouterModule } from '@angular/router';


@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    BodyComponent,
    CarouselComponent,
    TeamComponent,
    FAQComponent,
    UtilitiesComponent,
    FooterComponent,
    LandingPageComponent,
    ResizeDetectorDirective,
    VerticalTimelineComponent
  ],
  imports: [
    BrowserModule,
    AccordionModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
