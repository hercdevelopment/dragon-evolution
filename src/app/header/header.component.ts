import { Renderer2, Component, ElementRef, OnInit, ViewChild, AfterViewChecked, DoCheck } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit, DoCheck {

  @ViewChild('navbar') navbar!: ElementRef;
  @ViewChild('header') header!: ElementRef;
  @ViewChild('container') container!: ElementRef;
  containerHeight: number | undefined;

  constructor(private renderer: Renderer2) { }

  onToggleMenu() {
    this.navbar.nativeElement.classList.toggle('active');
  }

  ngOnInit() {
    
  }

  ngDoCheck() {
  
  if(this.header != undefined)
    this.renderer.setStyle(this.container.nativeElement, 'height', this.header.nativeElement.clientHeight + 1   + "px");
  }

}


