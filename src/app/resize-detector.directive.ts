import { Directive, ElementRef, HostListener } from '@angular/core';

@Directive({
  selector: '[appResizeDetector]'
})
export class ResizeDetectorDirective {

  public constructor(public element: ElementRef<HTMLElement>) { }

  public ngOnInit(): void {
    // Call detectResize as whe the element inits the windows resize event most likely won't trigger
    this.detectResize();
  }

  // if you need the windowDimensions add ", [$event]" to @HostListener and pass event to detectResize(event)
  @HostListener('window:resize')
  public detectResize(): number {
    console.log(this.element.nativeElement.clientHeight)
    return this.element.nativeElement.clientHeight;
  }


}
