import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BodyComponent } from './body/body.component';
import { LandingPageComponent } from './landing-page/landing-page.component';

const routes: Routes = [
  
  { path: 'dragonevolution', component: BodyComponent },
  { path: 'landing', component: LandingPageComponent },
  { path: '', redirectTo: 'landing', pathMatch: 'full' },
  { path: '**', component: BodyComponent}
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, {
      scrollPositionRestoration: 'enabled',
      anchorScrolling: 'enabled',
      scrollOffset: [0, 64],  // [x, y], 
      useHash: true
    })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
